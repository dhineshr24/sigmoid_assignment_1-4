package assignment_1;

import com.github.javafaker.Faker;

public class Employee {

    private final static Faker faker = new Faker();
    private int age;
    private String id;
    private String name;
    private String company;
    private String address;
    private String buildingCode;
    private String phoneNumber;

    public Employee() {
        this.id = faker.numerify("EMP####");
        this.name = faker.name().fullName();
        this.company = faker.company().name().split(",")[0];
        this.address = faker.address().streetAddress() + "-" + faker.address().cityName();
        this.age = Integer.parseInt(faker.numerify("##"));
        this.buildingCode = faker.color().name() + faker.numerify("##");
        this.phoneNumber = faker.phoneNumber().cellPhone();
    }

    //    GETTERS

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCompany() {
        return company;
    }

    public String getAddress() {
        return address;
    }

    public String getBuildingCode() {
        return buildingCode;
    }

    public int getAge() {
        return age;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }


    //      SETTERS
    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setBuildingCode(String buildingCode) {
        this.buildingCode = buildingCode;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    public String generateObjectCSV() {
        return this.getId() + "," + this.getName() + "," + this.getCompany() + "," + this.getAddress()
                + "," + this.getBuildingCode() + "," + this.getAge() + "," + this.getPhoneNumber();
    }
}
