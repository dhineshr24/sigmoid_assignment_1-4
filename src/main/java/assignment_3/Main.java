package assignment_3;

import protoPackage.BuildingProto;
import protoPackage.EmployeeProto;

import java.io.FileInputStream;
import java.io.IOException;

public class Main {
    public static void serializeCSVFiles(String type, String csvFilePath, String serializedFileOutputPath) throws IOException {
        ProtoBufUtils protoObj = new ProtoBufUtils(csvFilePath, serializedFileOutputPath);
        if (type.equals("Employee")) {
            protoObj.serializeEmployeeCSV();
        } else if (type.equals("Building")) {
            protoObj.serializeBuildingCSV();
        } else {
            System.out.println("Please provide correct type: Employee / Building");
        }
    }

    public static void printEmployeeData(String path) throws IOException {

        EmployeeProto.EmployeeDatabase deserialized = EmployeeProto.EmployeeDatabase.newBuilder()
                .mergeFrom(new FileInputStream(path)).build();
        int empCount = deserialized.getEmployeeCount();
        for (int i = 0; i < empCount; i++) {
            System.out.println(deserialized.getEmployee(i).getAllFields());
        }

    }

    public static void printBuildingData(String path) throws IOException {
        BuildingProto.BuildingDatabase deserialized = BuildingProto.BuildingDatabase.newBuilder()
                .mergeFrom(new FileInputStream(path)).build();
        int empCount = deserialized.getBuildingCount();
        for (int i = 0; i < empCount; i++) {
            System.out.println(deserialized.getBuilding(i).getAllFields());
        }
    }

    public static void main(String[] args) throws IOException {
        serializeCSVFiles(args[0], args[1], args[2]);
        if(args[0].equals("Building"))
            printBuildingData(args[2]);
        if (args[0].equals("Employee"))
            printEmployeeData(args[2]);
    }
}
